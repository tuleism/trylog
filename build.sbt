name := "trylog"

version := "0.1"

scalaVersion := "2.12.7"

val izumiVersion = "0.6.2"

// LogStage machinery
libraryDependencies ++= Seq(
  // LogStage API, you need it to use the logger
  "com.github.pshirshov.izumi.r2" %% "logstage-core" % izumiVersion,
  // json output
  "com.github.pshirshov.izumi.r2" %% "logstage-rendering-circe" % izumiVersion,
  // router from Slf4j to LogStage
  "com.github.pshirshov.izumi.r2" %% "logstage-adapter-slf4j" % izumiVersion,

  "cool.graph" % "cuid-java" % "0.1.1",

  "org.scalatest" %% "scalatest" % "3.0.1" % Test
)
