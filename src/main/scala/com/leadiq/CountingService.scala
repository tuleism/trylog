package com.leadiq

import cool.graph.cuid.Cuid
import logstage.IzLogger

import scala.concurrent.{ExecutionContext, Future}

class CountingService(logger: IzLogger) {

  def count(implicit ec: ExecutionContext): Future[Unit] = {
    val ctxLogger = logger("trackingId" -> Cuid.createCuid())

    Future.traverse(1 to 1000) { i =>
      Future {
        ctxLogger.info(s"At: $i")
        i
      }
    }
    .map(_ => ())
  }
}
