package com.leadiq

import com.github.pshirshov.izumi.logstage.api.rendering.RenderingPolicy
import com.github.pshirshov.izumi.logstage.sink.file.{FileService, FileSink}
import com.github.pshirshov.izumi.logstage.sink.file.models.{FileRotation, FileSinkConfig, LogFile}

class FileSinkImpl[F <: LogFile](override val renderingPolicy: RenderingPolicy
                                 , override val fileService: FileService[F]
                                 , override val rotation: FileRotation
                                 , override val config: FileSinkConfig) extends FileSink[F](renderingPolicy, fileService, rotation, config) {

  override def recoverOnFail(e: String): Unit = println
}
