package com.leadiq

import com.github.pshirshov.izumi.logstage.api.IzLogger
import com.github.pshirshov.izumi.logstage.api.Log.Level.Trace
import com.github.pshirshov.izumi.logstage.api.rendering.{RenderingOptions, StringRenderingPolicy}
import com.github.pshirshov.izumi.logstage.sink.ConsoleSink
import com.github.pshirshov.izumi.logstage.sink.file.FileServiceImpl
import com.github.pshirshov.izumi.logstage.sink.file.models.FileRotation.FileLimiterRotation
import com.github.pshirshov.izumi.logstage.sink.file.models.FileSinkConfig
import logstage.circe._

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.Random

object TryLog extends App {
  val jsonSink = ConsoleSink.json(prettyPrint = true)
  val consoleSink = ConsoleSink.text(colored = true)
  val fileService = new FileServiceImpl("logs")
  val fileSink = new FileSinkImpl(
    renderingPolicy = new StringRenderingPolicy(RenderingOptions(withExceptions = false, withColors = false)),
    fileService,
    rotation = FileLimiterRotation(limit = 10),
    config = FileSinkConfig.inBytes(1000)
  )

  val logger = IzLogger(Trace, List(jsonSink, consoleSink, fileSink))

  val justAnArg = "example"
  val justAList = List[Any](10, "green", "bottles")

  logger.trace(s"Argument: $justAnArg, another arg: $justAList")
  logger.info(s"Named expression: ${Random.nextInt() -> "random number"}")

  val ctxLogger = logger("userId" -> "user@google.com", "company" -> "acme")

  val service = new CountingService(logger)
  Await.ready(service.count(scala.concurrent.ExecutionContext.Implicits.global), 1.second)
}
