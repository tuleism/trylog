package com.leadiq

import com.github.pshirshov.izumi.logstage.api.IzLogger
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{MustMatchers, WordSpec}
import util.TestSink

import scala.concurrent.ExecutionContext.Implicits.global

class CountingServiceSpec extends WordSpec with MustMatchers with ScalaFutures {
  "CountingService" should {
    "log" in {
      // setup a test logger
      val testSink = new TestSink()
      val logger = IzLogger(IzLogger.Level.Info, testSink)

      // call count and await it
      val service = new CountingService(logger)
      service.count.futureValue

      val loggedMesssages = testSink.fetch().map { entry =>
        entry.message.template.raw(entry.message.args.map(_.value) :_*)
      }
      loggedMesssages must contain theSameElementsAs (1 to 10).map(i => s"At: $i")
    }
  }
}
